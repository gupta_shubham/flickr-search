# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a sample app which demonstrates Clean Architecture using MVVM.

It has the following Tech stack:

* ViewModel
* Dagger
* Glide
* Use case Pattern
* Kotlin Flow
* Coroutines
* Retrofit

![Screenshot 1](images/Screenshot_1616956360.png)
![Screenshot 2](images/Screenshot_1616956376.png)
![Screenshot 3](images/Screenshot_1616956388.png)

### How do I get set up? ###

Simply clone the repository in Android Studio and run on a device or emulator

### What is the architecture? ###

Clean Architecture has been used in the codebase. Following are the packages:

* **data**: It contains the repository which is responsible to get data through different sources (if needed), it has all the required models needed for api calling.
    - Technologies: Retrofit, Okhttp, Interceptors, Gson
    
* **di**: Contains classes needed for dependency injection having components, modules, qualifiers, scopes.
    - components: project comprises of 2 major components, app component and module component
    - modules: project has 4 molules for different purposes like context, flicker search, activity context and retrofit for networking
    - qualifier: ActivityContext (to identify activity context), ApplicationContext (to identify application context)
    - scope: ActivityScope and ApplicationScope
    
    -Technologies: Dagger2
    
* **domain**: The domain layer contains the mappers and use cases to implement business logics if needed
    - Technologies: Coroutine

* **presentation**: The presentation layer consists of the ui and the view models
    - ui: the UI part consists of Activities, Adapters, BaseActivity and the Recycler View Item Decoration
    - viewmodel: view model is designed in such a way that any number of arguments can be passed to the view model constructor using the factory design pattern
    
    - Technologies: ViewModel, Coroutines, StateFlow, Dagger2, Glide
