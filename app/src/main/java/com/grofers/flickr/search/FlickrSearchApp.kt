package com.grofers.flickr.search

import android.app.Activity
import android.app.Application
import com.grofers.flickr.search.di.module.ContextModule
import com.grofers.flickr.search.di.component.DaggerFlickrSearchAppComponent
import com.grofers.flickr.search.di.module.RetrofitModule
import com.grofers.flickr.search.di.component.FlickrSearchAppComponent


class FlickrSearchApp : Application() {
    lateinit var appComponent: FlickrSearchAppComponent
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerFlickrSearchAppComponent.builder()
            .contextModule(ContextModule(this))
            .retrofitModule(RetrofitModule(this))
            .build()
        appComponent.inject(this)
    }

    fun getApplicationComponent(): FlickrSearchAppComponent {
        return appComponent
    }

    companion object {
        fun get(activity: Activity): FlickrSearchApp {
            return activity.application as FlickrSearchApp
        }
    }
}
