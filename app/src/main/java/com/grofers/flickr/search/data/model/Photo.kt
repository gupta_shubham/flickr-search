package com.grofers.flickr.search.data.model


import com.google.gson.annotations.SerializedName

data class Photo(
    @SerializedName("farm")
    val farm: Int = 0,
    @SerializedName("id")
    val id: String = "",
    @SerializedName("isfamily")
    val isfamily: Int = 0,
    @SerializedName("isfriend")
    val isfriend: Int = 0,
    @SerializedName("ispublic")
    val ispublic: Int = 0,
    @SerializedName("owner")
    val owner: String = "",
    @SerializedName("secret")
    val secret: String = "",
    @SerializedName("server")
    val server: String = "",
    @SerializedName("title")
    val title: String = "",
    @SerializedName("url_l")
    val url_l: String?,
    @SerializedName("url_o")
    val url_o: String?,
    @SerializedName("url_c")
    val url_c: String?,
    @SerializedName("url_z")
    val url_z: String?,
    @SerializedName("url_n")
    val url_n: String?,
    @SerializedName("url_m")
    val url_m: String?,
    @SerializedName("url_q")
    val url_q: String?,
    @SerializedName("url_s")
    val url_s: String?,
    @SerializedName("url_t")
    val url_t: String?,
    @SerializedName("url_sq")
    val url_sq: String?
)