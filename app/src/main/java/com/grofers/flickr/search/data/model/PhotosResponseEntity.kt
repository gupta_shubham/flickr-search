package com.grofers.flickr.search.data.model


import com.google.gson.annotations.SerializedName

data class PhotosResponseEntity(
    @SerializedName("photos")
    val photos: Photos = Photos(),
    @SerializedName("stat")
    val stat: String = ""
)