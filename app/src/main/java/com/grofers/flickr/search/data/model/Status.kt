package com.grofers.flickr.search.data.model

enum class Status {
    LOADING, SUCCESS, ERROR
}