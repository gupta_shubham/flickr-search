package com.grofers.flickr.search.data.remote

import com.grofers.flickr.search.data.model.PhotosResponseEntity
import com.grofers.flickr.search.data.remote.retrofit.ApiService
import com.grofers.flickr.search.utils.Constants.API_KEY
import javax.inject.Inject

class PhotosRepository @Inject constructor() {

    @Inject
    lateinit var service: ApiService

    suspend fun loadRecentPhotos(): PhotosResponseEntity {
        return service.getRecentList(API_KEY)
    }

    suspend fun loadSearchPhotos(query: String): PhotosResponseEntity {
        return service.getSearchList(API_KEY, query)
    }


}
