package com.grofers.flickr.search.data.remote.retrofit

import com.grofers.flickr.search.data.model.PhotosResponseEntity
import com.grofers.flickr.search.utils.Constants.RECENT_LIST_ENDPOINT
import com.grofers.flickr.search.utils.Constants.SEARCH_LIST_ENDPOINT
import retrofit2.http.GET
import retrofit2.http.Query

internal const val URLS = "url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o"

interface ApiService {
    @GET(RECENT_LIST_ENDPOINT)
    suspend fun getRecentList(
        @Query("api_key") apiKey: String,
        @Query("extras") extras: String = URLS
    ): PhotosResponseEntity

    @GET(SEARCH_LIST_ENDPOINT)
    suspend fun getSearchList(@Query("api_key") apiKey: String,
                              @Query("text") text: String? = null,
                              @Query("extras") extras: String = URLS
    ): PhotosResponseEntity
}