package com.grofers.flickr.search.di.component

import com.grofers.flickr.search.FlickrSearchApp
import com.grofers.flickr.search.data.remote.retrofit.ApiService
import com.grofers.flickr.search.di.module.ContextModule
import com.grofers.flickr.search.di.module.FlickrSearchModule
import com.grofers.flickr.search.di.module.RetrofitModule
import com.grofers.flickr.search.di.scope.ApplicationScope
import dagger.Component


@ApplicationScope
@Component(modules = [ContextModule::class, RetrofitModule::class, FlickrSearchModule::class])
interface FlickrSearchAppComponent {
    var apiService: ApiService
    fun inject(flickrSearchApplication: FlickrSearchApp)
}