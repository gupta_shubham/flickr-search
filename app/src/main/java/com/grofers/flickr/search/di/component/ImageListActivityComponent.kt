package com.grofers.flickr.search.di.component

import com.grofers.flickr.search.di.scope.ActivityScope
import com.grofers.flickr.search.presentation.ui.photolist.ImageListActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [FlickrSearchAppComponent::class])
interface ImageListActivityComponent {
    fun injectListActivity(imageListActivity: ImageListActivity?)
}