package com.grofers.flickr.search.di.module

import android.content.Context
import com.grofers.flickr.search.di.qualifier.ApplicationContext
import com.grofers.flickr.search.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class ContextModule(private val context: Context) {
    @Provides
    @ApplicationScope
    @ApplicationContext
    fun provideContext(): Context {
        return context
    }
}