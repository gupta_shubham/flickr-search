package com.grofers.flickr.search.di.module

import com.grofers.flickr.search.data.remote.PhotosRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class FlickrSearchModule {
    @Singleton
    @Provides
    fun provideRemoteRepoInstance(): PhotosRepository {
        return PhotosRepository()
    }

}