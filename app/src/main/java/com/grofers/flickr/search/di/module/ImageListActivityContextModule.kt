package com.grofers.flickr.search.di.module

import android.content.Context
import com.grofers.flickr.search.di.qualifier.ActivityContext
import com.grofers.flickr.search.di.scope.ActivityScope
import com.grofers.flickr.search.presentation.ui.photolist.ImageListActivity
import dagger.Module
import dagger.Provides

@Module
class ImageListActivityContextModule(private val imageListActivity: ImageListActivity) {
    var context: Context = imageListActivity

    @Provides
    @ActivityScope
    fun providesImageListActivity(): ImageListActivity {
        return imageListActivity
    }

    @Provides
    @ActivityScope
    @ActivityContext
    fun provideContext(): Context {
        return context
    }

}