package com.grofers.flickr.search.di.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ActivityContext {

}