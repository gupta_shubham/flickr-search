package com.grofers.flickr.search.domain.mapper

data class MappedImageInfo(
    val id: String?,
    val title: String?,
    val flickrImg: String?
)

class MappedImageListData : ArrayList<MappedImageInfo>()
