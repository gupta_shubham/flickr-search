package com.grofers.flickr.search.domain.usecase

import com.grofers.flickr.search.data.remote.PhotosRepository
import com.grofers.flickr.search.domain.mapper.MappedImageListData
import com.grofers.flickr.search.utils.toMappedImageListData
import javax.inject.Inject

class RecentImageListUseCase @Inject constructor(private val repository: PhotosRepository) {
    suspend fun getRecentPhotosList(): MappedImageListData {
        val loadRecentPhotos = repository.loadRecentPhotos()
        return loadRecentPhotos.toMappedImageListData()
    }
}
