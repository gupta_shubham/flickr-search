package com.grofers.flickr.search.domain.usecase

import com.grofers.flickr.search.data.remote.PhotosRepository
import com.grofers.flickr.search.domain.mapper.MappedImageListData
import com.grofers.flickr.search.utils.toMappedImageListData
import javax.inject.Inject

class SearchImageListUseCase @Inject constructor(private val repository: PhotosRepository)  {
    suspend fun getSearchPhotosList(query: String): MappedImageListData {
        val loadSearchPhotos = repository.loadSearchPhotos(query)
        return loadSearchPhotos.toMappedImageListData()
    }
}