package com.grofers.flickr.search.presentation.ui.photolist

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.grofers.flickr.search.R
import com.grofers.flickr.search.FlickrSearchApp
import com.grofers.flickr.search.data.model.*
import com.grofers.flickr.search.data.remote.retrofit.ApiService
import com.grofers.flickr.search.di.component.DaggerImageListActivityComponent
import com.grofers.flickr.search.domain.mapper.MappedImageListData
import com.grofers.flickr.search.presentation.ui.BaseActivity
import com.grofers.flickr.search.presentation.ui.SpaceItemDecoration
import com.grofers.flickr.search.presentation.viewmodel.ImageListViewModelFactory
import com.grofers.flickr.search.presentation.viewmodel.ImageListViewModel
import com.grofers.flickr.search.utils.getQueryTextChangeStateFlow
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject


@ExperimentalCoroutinesApi
@FlowPreview
class ImageListActivity : BaseActivity() {
    private lateinit var onRefreshListener: SwipeRefreshLayout.OnRefreshListener
    private lateinit var observer: Observer<Response>
    private lateinit var searchView: SearchView

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var imageListViewModelFactory: ImageListViewModelFactory

    private lateinit var viewModel: ImageListViewModel

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var errorMessageTextView: TextView
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initInjector()
        initUI()
        initViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.filter_menu, menu)
        searchView = menu?.findItem(R.id.search)?.actionView as SearchView
        searchView.apply {
            setIconifiedByDefault(false)
        }
        if (::viewModel.isInitialized)
            viewModel.getSearchImageList(searchView.getQueryTextChangeStateFlow())

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return true
    }

    override fun initInjector() {
        val applicationComponent = FlickrSearchApp.get(this).getApplicationComponent()
        DaggerImageListActivityComponent.builder()
            .flickrSearchAppComponent(applicationComponent)
            .build().injectListActivity(this)
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            imageListViewModelFactory
        ).get(ImageListViewModel::class.java)
        observer = Observer { response: Response ->
            when (response.status) {
                Status.LOADING -> {
                    swipeRefreshLayout.setOnRefreshListener(null)
                    swipeRefreshLayout.isRefreshing = true
                }
                Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                    swipeRefreshLayout.setOnRefreshListener(onRefreshListener)
                    run {
                        recyclerView.visibility = View.GONE
                        errorMessageTextView.visibility = View.VISIBLE
                    }
                }
                else -> {
                    updateRecyclerViewData(response)
                    swipeRefreshLayout.setOnRefreshListener(onRefreshListener)
                }
            }
        }
        viewModel.getRecentImages().observe(this,observer)

        viewModel.searchImageLiveData.observe(this, observer)

        if (::searchView.isInitialized)
            viewModel.getSearchImageList(searchView.getQueryTextChangeStateFlow())
    }

    private fun initUI() {
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)
        errorMessageTextView = findViewById(R.id.error_message_view)
        swipeRefreshLayout.isRefreshing = true
        recyclerView = findViewById(R.id.recyclerView)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = ImageListAdapter(MappedImageListData())
        recyclerView.addItemDecoration(SpaceItemDecoration(LinearLayoutManager.VERTICAL))
        onRefreshListener = SwipeRefreshLayout.OnRefreshListener {
            viewModel.getRecentImages().observe(this, observer)
        }
    }

    private fun updateRecyclerViewData(response: Response) {
        response.data?.let {
            (recyclerView.adapter as ImageListAdapter).updateList(it)
        }
        recyclerView.visibility = View.VISIBLE
        errorMessageTextView.visibility = View.GONE
        swipeRefreshLayout.isRefreshing = false
    }
}

