package com.grofers.flickr.search.presentation.ui.photolist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.grofers.flickr.search.R
import com.grofers.flickr.search.domain.mapper.MappedImageListData

class ImageListAdapter(private var data: MappedImageListData) :
    RecyclerView.Adapter<ImageListAdapter.ImageListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_image_list, parent, false)
        return ImageListViewHolder(view)
}

    override fun onBindViewHolder(holder: ImageListViewHolder, position: Int) {
        data[position].flickrImg.let {
            Glide.with(holder.itemView.context).load(it)
                .placeholder(R.drawable.no_image)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(holder.flickrImageView)
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun updateList(mappedImageListData: MappedImageListData) {
        data = mappedImageListData
        notifyDataSetChanged()
    }


    class ImageListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var flickrImageView: ImageView
        init {
            itemView.run {
                flickrImageView = findViewById(R.id.flickr_image)
            }
        }


    }
}
