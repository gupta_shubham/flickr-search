package com.grofers.flickr.search.presentation.viewmodel

import androidx.lifecycle.*
import com.grofers.flickr.search.data.model.Response
import com.grofers.flickr.search.data.model.Response.Companion.loading
import com.grofers.flickr.search.data.model.Response.Companion.success
import com.grofers.flickr.search.domain.usecase.RecentImageListUseCase
import com.grofers.flickr.search.domain.usecase.SearchImageListUseCase
import com.grofers.flickr.search.utils.BaseDispatcherProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class ImageListViewModel @Inject constructor(
    private val usecaseRecent: RecentImageListUseCase,
    private val usecaseSearch: SearchImageListUseCase,
    private val dispatcherProvider: BaseDispatcherProvider
) : ViewModel() {

    fun getRecentImages(): LiveData<Response> =
        liveData {
            emit(loading())
            val recentPhotosList = usecaseRecent.getRecentPhotosList()
            emit(success(recentPhotosList))
        }

    val searchImageLiveData: LiveData<Response>
        get() = _searchImageListData

    private var _searchImageListData: MutableLiveData<Response> = MutableLiveData()

    fun getSearchImageList(queryTextChangeStateFlow: StateFlow<String>) {
        viewModelScope.launch {
            queryTextChangeStateFlow
                .debounce(500)
                .distinctUntilChanged()
                .flatMapLatest { query ->
                    if (query.isNotEmpty())
                        flow {
                            emit(usecaseSearch.getSearchPhotosList(query))
                        }.catch {
                            emit(usecaseRecent.getRecentPhotosList())
                        } else flow {
                        emit(usecaseRecent.getRecentPhotosList())
                    }
                }
                .flowOn(dispatcherProvider.default())
                .collect {
                    _searchImageListData.value = success(it)
                }
        }

    }

}
