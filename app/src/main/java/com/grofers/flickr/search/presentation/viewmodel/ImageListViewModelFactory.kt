package com.grofers.flickr.search.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.grofers.flickr.search.domain.usecase.RecentImageListUseCase
import com.grofers.flickr.search.domain.usecase.SearchImageListUseCase
import com.grofers.flickr.search.utils.DispatcherProvider
import javax.inject.Inject

class ImageListViewModelFactory @Inject constructor(
    private val useCaseRecent: RecentImageListUseCase,
    private val useCaseSearch: SearchImageListUseCase,
    private val dispatcherProvider: DispatcherProvider
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ImageListViewModel::class.java)) {
            return ImageListViewModel(useCaseRecent, useCaseSearch, dispatcherProvider) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
