package com.grofers.flickr.search.utils

import kotlinx.coroutines.CoroutineDispatcher

interface BaseDispatcherProvider {
    fun io() : CoroutineDispatcher
    fun default() : CoroutineDispatcher
    fun ui() : CoroutineDispatcher
}