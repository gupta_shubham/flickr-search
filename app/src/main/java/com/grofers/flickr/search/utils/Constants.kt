package com.grofers.flickr.search.utils

object Constants {
    const val BASE_URL = "https://www.flickr.com/"
    const val RECENT_LIST_ENDPOINT = "services/rest/?method=flickr.photos.getRecent&nojsoncallback=1&format=json"
    const val SEARCH_LIST_ENDPOINT = "services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1"
    const val API_KEY = "fb7dbf0bd884a946e8af47addf706e37"



}