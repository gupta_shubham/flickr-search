package com.grofers.flickr.search.utils
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class DispatcherProvider @Inject constructor() : BaseDispatcherProvider{
    /**
     * IO thread pool scheduler
     */
    override fun io(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    /**
     * Computation thread pool scheduler
     */
    override fun default(): CoroutineDispatcher {
        return Dispatchers.Default
    }

    /**
     * Main Thread scheduler
     */
    override fun ui(): CoroutineDispatcher {
        return Dispatchers.Main
    }

}