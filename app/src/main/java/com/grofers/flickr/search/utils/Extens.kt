package com.grofers.flickr.search.utils

import androidx.appcompat.widget.SearchView
import com.grofers.flickr.search.data.model.Photo
import com.grofers.flickr.search.data.model.PhotosResponseEntity
import com.grofers.flickr.search.domain.mapper.MappedImageInfo
import com.grofers.flickr.search.domain.mapper.MappedImageListData
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow


fun PhotosResponseEntity.toMappedImageListData(): MappedImageListData {
    val mappedImageListData = MappedImageListData()
    photos.photo.all { info -> mappedImageListData.add(info.toMappedPhotoInfo()) }
    return mappedImageListData
}

fun Photo.toMappedPhotoInfo(): MappedImageInfo {
    return MappedImageInfo(id, title, url_l ?: url_o ?: url_c ?: url_z ?: url_n ?: url_m
    ?: url_q ?: url_s ?: url_t ?: url_sq)
}

fun SearchView.getQueryTextChangeStateFlow(): StateFlow<String> {

    val query = MutableStateFlow("")

    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return true
        }

        override fun onQueryTextChange(newText: String): Boolean {
            query.value = newText
            return true
        }
    })

    return query

}